1. kerjakan bagian ES6(dari awal sampai promise) di bagian JavaScript Algorithms and Data Structures Certification di freecodecamp(screenshoot bukti course ES6 sudah selesai). *promise akan kita bahas di materi asynchronous
> ![ES6 FreeCodeCamp!](/images/Screenshot%202021-12-25%20184138.png "ES6")

2. ubah code di codesandbox berikut (https://codesandbox.io/s/confident-thompson-w7ezb?file=/src/index.js:0-253) agar menggunakan syntax ES6
> ES6.js

3. kerjakan tugas pada file pdf JavaScript OOP halaman terakhir (file ada di google classroom)
> oop.js

4. buat sebuah todo app dengan fungsionalitas sebagai berikut:
    * user dapat melihat list todo (ordered list)
    * user dapat menambah item ke list todo (form, input, button)
    * list todo yang muncul memiliki ketentuan sebagai berikut:
      * list todo index 0 memiliki warna teks merah
      * list todo index 1 memiliki warna teks hijau
      * list todo index 2 memiliki warna teks biru
      * list todo index 3 memiliki warna teks merah
      * list todo index 4 memiliki warna teks hijau
      * dan seterusnya
  > todo.html