class Student {
  constructor() {
    this.name = '';
    this.age = null;
    this.dateOfBirth = '';
    this.gender = '';
    this.studentId = '';
    this.hobbies = [];
  }

  getData() { 
    return this
  }

  setName(name) {
    typeof name === 'string' ? this.name = name : console.error(Error());
  }

  setAge(age) {
    typeof age === 'number' ? this.age = age : console.error(Error());
  }

  setDateOfBirth (date) {
    typeof date === 'string' ? this.dateOfBirth = date : console.error(Error());
  }

  setGender (gender) {
    gender === 'Male' || gender === 'Female' ? this.gender = gender : console.error(Error());
  }

  setId(id) {
    typeof id === 'string' || typeof id === 'number' ? this.studentId = id : console.error(Error());
  }

  addHobby (hobby) {
    typeof hobby === 'string' ? this.hobbies.push(hobby) : console.error(Error());
  }
  
  removeHobby (hobby) {
    if (typeof hobby === 'string') {
      const idx = this.hobbies.indexOf(hobby);
      this.hobbies.splice(idx, 1);
    } else {
      console.error(Error());
    }
  }

  
}

const ardhi = new Student()
ardhi.setName('D.N. Ardhi')
ardhi.setAge(22)
ardhi.setGender('Male')
ardhi.setId(12345)
ardhi.addHobby('tidur')
ardhi.addHobby('makan')
ardhi.addHobby('minum')
console.log(ardhi.getData());
ardhi.removeHobby('tidur')
console.log(ardhi.getData());